#include <stdio.h>

int mimatriz [5][5] ;
int i ;
int j ;


void inicializa ()
{
	int i ;
	int j ;

	i = 1 ;
	while (i < 5) {
		j = 1 ;
		while (j < 5) {
			mimatriz [i-1][j-1] = i + 2*j ;
			j = j + 1 ;
		}
		i = i + 1 ;
	}	
}


void imprime () 
{
	int w;
	i = 1 ;
	while (i < 5) {
		j = 1 ;
		while (j < 5) {
			w = mimatriz [i-1][j-1] ;
			printf ("%d ", w) ;
			j = j + 1 ;
		}
		//puts ("\n") ;    // Esto igual no funiona en Forth, se simula con la siguiente embebida
//@ cr
		i = i + 1 ;
	}
}


void transpuesta () 
{
    int i ;
    int j ;
    int aux ;
    
    i = 0 ;
    do {
       j = i+1 ; 
       while (j < 5) {
           aux = mimatriz [i][j] ;
           mimatriz [i][j] = mimatriz [j][i] ;
           mimatriz [j][i] = aux ;
           j = j +1 ;
       } 
       i = i + 1 ;
    } while  (i < 5) ;
}

void pruebasNOSOTROS () {
	int i, j;
//int mimatriz [5][5] ;
	i = 1;
	j = 0;

	mimatriz [0][0] = 23;
	while (j < 5) {
		mimatriz [0][j] = 23;
		++j;
    }
    while (i < 5) {
		mimatriz [i][0] = mimatriz [i-1][2];
		++i;
    } 
}


main () {
	puts ("Matriz sin inicializar ") ;
//@ cr
	imprime () ;
	inicializa () ;
//@ cr
	puts ("Matriz inicializada ") ;
//@ cr
	imprime () ;
	transpuesta () ;
//@ cr
	puts ("Matriz transpuesta ") ;
//@ cr
	imprime () ;

	pruebasNOSOTROS ();
	imprime ();
	
//    system ("pause") ;
}

//@ main



