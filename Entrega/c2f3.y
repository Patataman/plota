%{
#include <stdio.h>
#include <string.h>           // declaraciones para cadenas
#include <stdlib.h>           // declaraciones para exit ()
#define FF fflush(stdout);    // para forzar la impresion inmediata

char *nomb_func;
int contar = 0;
int contarArgs = 0;

//Tabla de simbolos
typedef struct {
    char nombre[30];
    char argumentos[2][30];
} Entrada;

Entrada tabla_simbolos[40];

/*
    Busca en la tabla de símbolos el nombre y devuelve 1 si está
    y un 0 si no está.
    
    func: Indica si se quiere buscar función (1) o varible (0)
*/
int searchVar (char *nombre) {
    if (contar == 0) { return 0; }
    int w = 0; 
    for (w=0;w<40 && (contar != 0 && contar < 40);w++) {
        if (strcmp(nombre,tabla_simbolos[w].nombre) == 0){
            return 1;
        }
    }

    return 0;
}

/*
    Busca en la tabla de símbolos el nombre y devuelve un 1 si está
    y un -1 si no está.

*/
int searchFunc (char *nombre, char *argu) {
    if (contar == 0) { return 0; }
    int w = 0; 
    for (w=0;w<40 && (contar != 0 && contar < 40);w++) {
        if (strcmp(nombre,tabla_simbolos[w].nombre) == 0){
            int argNum;
            for (argNum = 0; argNum < 2; argNum++){
                if (strcmp(argu,tabla_simbolos[w].argumentos[argNum]) == 0){
                    return 1;
                }
            }
            return -1;
        }
    }

    return -1;
}

void generarArgumentos() {
    int i;
    //Se lee el argumentos, si se llega a \0 es que tiene nombre válido, si se llega a 30, es que estaba vacío.
    for (i=0;i<3;i++){
        //Si se encuentra argumento válido
        if (tabla_simbolos[contar-1].argumentos[i][0] != '\0'){
            printf(" %s_%s !",nomb_func,tabla_simbolos[contar-1].argumentos[i]);
        }
    }
    printf("\n");
}

//HACER TABLA SIMBOLOS PARA VARIABLES GLOBALES
%}
%union {                      // El tipo de la pila tiene caracter dual
      int valor ;             // - valor numerico de un NUMERO
      char *cadena ;          // - para pasar los nombres de IDENTIFES
}
%token <valor> NUMERO         // Todos los token tienen un tipo para la pila
%token <cadena> IDENTIF       // Identificador=variable
%token <cadena> INTEGER       // identifica la definicion de un entero
%token <cadena> STRING
%token <cadena> PUTS
%token <cadena> DO            // AÑADIMOS EL TOKEN DO
%token <cadena> WHILE
%token <cadena> IF            // AÑADIMOS LOS TOKENS IF THEN ELSE
%token <cadena> THEN
%token <cadena> ELSE
%token <cadena> OR
%token <cadena> AND
%token <cadena> EQUAL
%token <cadena> NEQUAL
%token <cadena> LT
%token <cadena> GT
%token <cadena> MASMAS
%token <cadena> MENOMENO
%token <cadena> PORIGUAL   
%token <cadena> PRINTF
%token <cadena> FUNC
%token <cadena> RET
%token <cadena> VOID
%token <cadena> MAIN          // identifica el comienzo del proc. main
%right MASIGUAL MENOIGUAL
%right '='
%left OR          //necesario que se tengan las operaciones realizadas para comparar
%left AND 
%left '|'
%left '&'
%left EQUAL NEQUAL
%left LT GT '<' '>'
%left '+' '-'                 // menor orden de precedencia
%left '*' '/'                 // orden de precedencia intermedio
%left '%'               //modulo 
%right '!'
%right SIGNO_UNARIO
%right MASMAS MENOMENO          // mayor orden de precedencia  

%%
                                          // Seccion 3 Gramatica - Semantico
programa:      before_main principal 		{ ; }
             ;

principal:      MAIN {nomb_func = $1;}				
                 '(' ')' '{' var_locales { printf (": main \n"); FF; } codigo '}'	 { printf (" ;\n"); FF; }
             ;
  
before_main:        /* lambda */		{ ; }
             | INTEGER IDENTIF varM_globales		{ strcpy(tabla_simbolos[contar].nombre,$2);
                                          contar++;
                                          printf("variable %s\n", $2); FF; }
                 before_main

                    /* Vectores */
             | INTEGER IDENTIF '[' NUMERO ']' varM_globales { strcpy(tabla_simbolos[contar].nombre,$2);
                                                    contar++;
                                                    printf("variable %s %d cells allot\n", $2, $4);}
                 before_main

                    /* Matrices */
             | INTEGER IDENTIF '[' NUMERO ']' '['NUMERO ']' varM_globales {   strcpy(tabla_simbolos[contar].nombre,$2);
                                                                    contar++;
                                                                    printf("variable %s %d %d * %d + cells allot\n", $2, $4, $7, $4);
                                                                    int i;
                                                                    for (i = 0 ; i < $4 ; i++) {
                                                                     printf (" %d  %d %d *  + cells %s + %d cells %s + !\n", $4, i, $7, $2, i, $2) ;
                                                                    } 
                                                                }
                before_main

                    /* Funciones*/
             | INTEGER IDENTIF {nomb_func = $2; strcpy(tabla_simbolos[contar].nombre,$2);} 
             '(' decl_args ')' 
             '{' var_locales { printf (": %s ", $2); generarArgumentos(); } 
                codigo RET expresion ';' {printf(" ;\n"); } '}'
                def_func 

             | VOID IDENTIF {nomb_func = $2; strcpy(tabla_simbolos[contar].nombre,$2);}
             '(' decl_args ')' 
             '{' var_locales { printf (": %s", $2); generarArgumentos(); } codigo '}' { printf(" ;\n"); } 
                def_func 
             ;

varM_globales: ';'
             | ',' IDENTIF varM_globales                { strcpy(tabla_simbolos[contar].nombre,$2);
                                          contar++;
                                          printf("variable %s\n", $2); FF; }
             | ',' IDENTIF '[' NUMERO ']' varM_globales { strcpy(tabla_simbolos[contar].nombre,$2);
                                                    contar++;
                                                    printf("variable %s %d cells allot\n", $2, $4);}
             | ',' IDENTIF '[' NUMERO ']' '['NUMERO ']' varM_globales { strcpy(tabla_simbolos[contar].nombre,$2);
                                                                        contar++;
                                                                        printf("variable %s %d %d * %d + cells allot\n", $2, $4, $7, $4);
                                                                        int i;
                                                                        for (i = 0 ; i < $4 ; i++) {
                                                                            printf (" %d %d %d *  + cells %s + %d cells %s + !\n", $4, i, $7, $2, i, $2) ;
                                                                        } 
                                                                    }
             ;

var_locales:     /* lambda */          { ; }
             | INTEGER IDENTIF varM_locales      { printf ("variable %s_%s\n", nomb_func,$2); FF; }
                 var_locales
                 /* Vectores */
             | INTEGER IDENTIF '[' NUMERO ']' varM_locales {printf("variable %s_%s %d cells allot\n",nomb_func, $2, $4);}
                 var_locales
                 /* Matrices */
             | INTEGER IDENTIF '[' NUMERO ']' '['NUMERO ']' varM_locales {printf("variable %s_%s %d %d * %d + cells allot\n",nomb_func, $2, $4, $7, $4);
                                                                            int i;
                                                                            for (i = 0 ; i < $4 ; i++) {
                                                                                printf (" %d %d %d *  + cells %s_%s + %d cells %s + !\n", $4, i, $7, nomb_func, $2, i, nomb_func, $2) ;
                                                                            } 
                                                                        }
                 var_locales                                            
             ;
             /* Delcasacion multiple*/
varM_locales: ';'
             | ',' IDENTIF varM_locales                { printf ("variable %s_%s\n", nomb_func,$2); FF; }
             | ',' IDENTIF '[' NUMERO ']' varM_locales {printf("variable %s_%s %d cells allot\n",nomb_func, $2, $4);}
             | ',' IDENTIF '[' NUMERO ']' '['NUMERO ']' varM_locales {printf("variable %s_%s %d %d * %d + cells allot\n",nomb_func, $2, $4, $7, $4);
                                                                        int i;
                                                                        for (i = 0 ; i < $4 ; i++) {
                                                                            printf (" %d %d %d *  + cells %s_%s + %d cells %s + !\n", $4, i, $7, nomb_func, $2, i, nomb_func, $2) ;
                                                                        } 
                                                                    }
             ;

    //Para declarar funciones
def_func:       /* lambda */            { ; }
             | INTEGER IDENTIF {nomb_func = $2; strcpy(tabla_simbolos[contar].nombre,$2);} 
             '(' decl_args ')' 
             '{' var_locales { printf (": %s ", $2); generarArgumentos(); } 
                codigo RET expresion ';' {printf(" ;\n"); FF; } '}'
                def_func 

             | VOID IDENTIF {nomb_func = $2; strcpy(tabla_simbolos[contar].nombre,$2);}
             '(' decl_args ')' 
             '{' var_locales { printf (": %s", $2); generarArgumentos(); } codigo '}' { printf(" ;\n"); } 
                def_func 
             ;

decl_args:      /* lambda */ { contar++; }
             | INTEGER IDENTIF {strcpy(tabla_simbolos[contar].argumentos[contarArgs],$2);
                                contarArgs=0; contar++; 
                                printf ("variable  %s_%s\n", nomb_func, $2);}
             | INTEGER IDENTIF {strcpy(tabla_simbolos[contar].argumentos[contarArgs],$2);
                                contarArgs++; 
                                printf ("variable %s_%s ", nomb_func, $2);} 
              ',' decl_args
             ;  
/*
asigMultiple: expresion ';'                 { if(searchVar($1) == 1){
                                                printf(" %s !\n", $1);
                                              } else {
                                                printf (" %s_%s !\n", nomb_func, $1);
                                              }
                                            }
             | IDENTIF '=' asigMultiple     { if(searchVar($1) == 1){
                                                printf(" %s @", $3);
                                              } else {
                                                printf (" %s_%s @", nomb_func, $3);
                                              }
                                            }
             ;
*/
codigo:               /* lambda */ 		{ ; }
             | IDENTIF '=' expresion ';' 	{ if (searchFunc(nomb_func, $1) == 1) {
                                                printf (" %s_%s !\n", nomb_func, $1);
                                              }
                                              else {
                                                    if(searchVar($1) == 1){
                                                      printf(" %s !\n", $1);
                                                    } else {
                                                      printf (" %s_%s !\n", nomb_func, $1);
                                                    }
                                                }
                                            }
                 codigo 

                 /* +=, -=  y *= */
             | IDENTIF MASIGUAL { if(searchVar($1) == 1){
                                    printf(" %s @ ", $1);
                                  } else {
                                    printf (" %s_%s @ ", nomb_func, $1);
                                  }
                                }
                 expresion ';'  { if(searchVar($1) == 1){
                                    printf(" + %s !\n", $1);
                                  } else {
                                    printf (" + %s_%s !\n", nomb_func, $1);
                                  }
                                }
                codigo

             | IDENTIF MENOIGUAL  { if(searchVar($1) == 1){
                                    printf(" %s @ ", $1);
                                    } else {
                                        printf (" %s_%s @ ", nomb_func, $1);
                                    }
                                  }
                 expresion ';'  { if(searchVar($1) == 1){
                                    printf(" - %s !\n", $1);
                                  } else {
                                    printf (" - %s_%s !\n", nomb_func, $1);
                                  }
                                }
                codigo

             | IDENTIF PORIGUAL  { if(searchVar($1) == 1){
                                    printf(" %s @ ", $1);
                                    } else {
                                        printf (" %s_%s @ ", nomb_func, $1);
                                    }
                                  }
                 expresion ';'  { if(searchVar($1) == 1){
                                    printf(" * %s !\n", $1);
                                  } else {
                                    printf (" * %s_%s !\n", nomb_func, $1);
                                  }
                                }
                codigo 

                /* Asignacion en vectores: vec[expr1] = expr2 */
             | IDENTIF '[' expresion ']' '=' expresion ';' { if(searchVar($1) == 1){
                                                            printf(" %s rot cells + !\n", $1);
                                                          } else {
                                                            printf (" %s_%s rot cells + !\n", nomb_func, $1);
                                                          }
                                                        }
                codigo

            | IDENTIF '[' expresion ']' '[' expresion ']' '=' expresion ';'  { if(searchVar($1) == 1){
                                                                            printf(" rot rot swap cells %s + @ swap cells + !\n", $1);
                                                                          } else {
                                                                            printf (" rot rot swap cells %s_%s + @ swap cells + !\n", nomb_func, $1);
                                                                          }
                                                                        }
                codigo

                /* ++ y -- prefix*/
             | MASMAS IDENTIF ';'   { if(searchVar($2) == 1){
                                        printf(" %s @ 1 + %s !\n", $2, $2);
                                      } else {
                                        printf (" %s_%s @ 1 + %s_%s !\n", nomb_func, $2, nomb_func, $2);
                                      }
                                    }      
                codigo

             | MENOMENO IDENTIF ';'     { if(searchVar($2) == 1){
                                            printf(" %s @ 1 - %s !\n", $2, $2);
                                          } else {
                                            printf (" %s_%s @ 1 - %s_%s !\n", nomb_func, $2, nomb_func, $2);
                                          }
                                        }
                codigo

	         | PUTS '(' STRING ')' ';'		{ printf (" .\" %s \"\n", $3) ; }
                 codigo
             | WHILE 			{ printf (" begin ") ; }
			'(' expresion ')' 	{ printf (" while\n  ") ; }
			'{' codigo '}'   	{ printf (" repeat \n") ; }
                 codigo 

                 //do { <codigo> } while (<expr>) -----> begin [codigo] [expr] while repeat
             | DO               { printf (" begin ") ; }
             '{' codigo '}' 
             WHILE              
             '(' expresion ')' ';'  { printf (" while\n repeat\n") ; }
                codigo

                //printf(string, expr) --> [expr] .
             | PRINTF '(' STRING ',' expresion ')' ';' { printf (" .\n") ; }
                codigo 

                //if (<expr>) { <codigo> }   --->  [expr] if [codigo] then
                //if (<expr>) { <codigo1> } else { <codigo2> } --->  [expr] if [codigo] else [codigo2] then

             | IF               //Versíon sin ambiguedad
            '(' expresion ')'   { printf (" if ") ; }
            '{' codigo '}'      
                ifthenelse

                //Traducción de uso de funciones en el código
             | IDENTIF '(' param ')' ';' { printf(" %s\n", $1 ); }
                 codigo
             ;

ifthenelse:    { printf (" then \n") ; }    codigo
             |  ELSE            { printf (" else ") ; }
            '{' codigo '}'      { printf (" then \n") ; }
                 codigo 
             ; 

param:         /* lambda */        { ; }
             | termino             { ; }
             | termino ',' param   { ; }
             ;                       


expresion:     termino              { ; }
             | expresion '+' expresion      { printf (" +") ; }
             | expresion '-' expresion      { printf (" -") ; }
             | expresion '*' expresion      { printf (" *") ; }
             | expresion '/' expresion      { printf (" /") ; }
             | expresion '|' expresion      { printf (" or") ; }
             | expresion OR expresion       { printf (" or") ; }
             | expresion '&' expresion      { printf (" and") ; }
             | expresion AND expresion      { printf (" and") ; }
             | expresion '>' expresion      { printf (" >") ; }
             | expresion '<' expresion      { printf (" <") ; }
             | expresion LT expresion       { printf (" <=") ; }
             | expresion GT expresion       { printf (" >=") ; }
             | expresion '%' expresion      { printf (" mod") ; }
             | expresion '=' expresion      { printf (" =") ; }
             | expresion EQUAL expresion    { printf (" =") ; }
             | expresion '!' expresion      { printf (" 0") ; }
             | expresion NEQUAL expresion   { printf (" = 0=") ; }
             ;

termino:        operando                { ; }
             |  '+' operando %prec SIGNO_UNARIO { ; }
             |  '-' operando %prec SIGNO_UNARIO { printf ("negate ") ; }
             ;

operando:      IDENTIF          { if (searchFunc(nomb_func, $1) == 1) {
                                            printf (" %s_%s @", nomb_func, $1);
                                        }
                                  else {
                                        if(searchVar($1) == 1){
                                          printf(" %s @", $1);
                                        } else {
                                          printf (" %s_%s @", nomb_func, $1);
                                        }
                                    }
                                }


             | NUMERO               { printf (" %d", $1) ; }
             | '(' expresion ')'        { ; }

                     /* ++ */
             | MASMAS IDENTIF       { if(searchVar($2) == 1){
                                        printf(" %s @ 1 + %s ! %s @", $2, $2, $2);
                                      } else {
                                        printf (" %s_%s @ 1 + %s_%s ! %s_%s @", nomb_func, $2, nomb_func, $2, nomb_func, $2);
                                      }
                                    }
                     /* -- */
             | MENOMENO IDENTIF     { if(searchVar($2) == 1){
                                        printf(" %s @ 1 - %s !", $2, $2);
                                      } else {
                                        printf (" %s_%s @ 1 - %s_%s !", nomb_func, $2, nomb_func, $2);
                                      }
                                    }

                /* Posición de un vector como operando */
             | IDENTIF '[' expresion ']'    { if(searchVar($1) == 1){
                                                printf(" %s", $1);
                                              } else {
                                                printf (" %s_%s", nomb_func, $1);
                                              }
                                              printf(" swap");
                                            } 
                                            { printf(" cells + @"); }

                /* Posición de una matriz como operando */
             | IDENTIF '[' expresion ']' '['  { if(searchVar($1) == 1){
                                                printf(" cells %s + @", $1);
                                              } else {
                                                printf (" cells %s_%s + @", nomb_func, $1);
                                              }
                                            }
                 expresion ']' { printf(" cells + @"); }

             | IDENTIF  '(' param ')' { printf(" %s", $1 ); }

             ;

%%
                            // SECCION 4    Codigo en C
int n_linea = 1 ;

int yyerror (mensaje)
char *mensaje ;
{
    fprintf (stderr, "%s en la linea %d\n", mensaje, n_linea) ;
    printf ( "bye\n") ;
}

char *mi_malloc (int nbytes)       // reserva n bytes de memoria dinamica
{
    char *p ;
    static long int nb = 0;        // sirven para contabilizar la memoria
    static int nv = 0 ;            // solicitada en total

    p = malloc (nbytes) ;
    if (p == NULL) {
         fprintf (stderr, "No queda memoria para %d bytes mas\n", nbytes) ;
         fprintf (stderr, "Reservados %ld bytes en %d llamadas\n", nb, nv) ;
         exit (0) ;
    }
    nb += (long) nbytes ;
    nv++ ;

    return p ;
}


/***************************************************************************/
/********************** Seccion de Palabras Reservadas *********************/
/***************************************************************************/

typedef struct s_pal_reservadas { // para las palabras reservadas de C
    char *nombre ;
    int token ;
} t_reservada ;

t_reservada pal_reservadas [] = { // define las palabras reservadas y los
    "main",        MAIN,           // y los token asociados
    "int",         INTEGER,
    "puts",        PUTS,
    "while",	   WHILE,
    "do",          DO, //////////////////////// AÑADIMOS COMO PALABRA RESERVADA 'DO'
    "if",          IF,
    "then",        THEN,
    "else",        ELSE,
    "&&",          AND,
    "||",          OR,
    "<=",          LT, //(less than)
    ">=",          GT, //(greater than)
    "==",          EQUAL,
    "!=",          NEQUAL,
    "++",          MASMAS,
    "+=",          MASIGUAL,
    "--",          MENOMENO,
    "-=",          MENOIGUAL,
    "*=",          PORIGUAL,
    "printf",      PRINTF,
    "return",      RET,
    "void",        VOID,
    NULL,          0               // para marcar el fin de la tabla
} ;

t_reservada *busca_pal_reservada (char *nombre_simbolo)
{                                  // Busca n_s en la tabla de pal. res.
                                   // y devuelve puntero a registro (simbolo)
    int i ;
    t_reservada *sim ;

    i = 0 ;
    sim = pal_reservadas ;
    while (sim [i].nombre != NULL) {
           if (strcmp (sim [i].nombre, nombre_simbolo) == 0) {
                                         // strcmp(a, b) devuelve == 0 si a==b
                 return &(sim [i]) ;
             }
           i++ ;
    }

    return NULL ;
}

 
/***************************************************************************/
/******************* Seccion del Analizador Lexicografico ******************/
/***************************************************************************/

char *genera_cadena (char *nombre)     // copia el argumento a un
{                                      // string en memoria dinamica
      char *p ;
      int l ;

      l = strlen (nombre)+1 ;
      p = (char *) mi_malloc (l) ;
      strcpy (p, nombre) ;

      return p ;
}


int yylex ()
{
    int i ;
    unsigned char c ;
    unsigned char cc ;
    char ops_expandibles [] = "=!<>&|%+-*/" ;
    char cadena [256] ;
    t_reservada *simbolo ;
    char func [256] ; //Para conocer el nombre de la funcion en cuestion

    do {
         c = getchar () ;

         if (c == '#') {	// Ignora las líneas que empiezan por #  (#define, #include)
             do {		//	OJO que puede funcionar mal si una linea contiene #
                 c = getchar () ;	
             } while (c != '\n') ;
         }

         if (c == '/') {	// Si la linea contiene un / puede ser inicio de comentario
             cc = getchar () ;
             if (cc != '/') {   // Si el siguiente char es /  es un comentario, pero...
                 ungetc (cc, stdin) ;
             } else {
                 c = getchar () ;	// ...
                 if (c == '@') {	// Si es la secuencia //@  ==> transcribimos la linea
                      do {		// Se trata de codigo inline (Codigo Forth embebido en C)
                          c = getchar () ;
                          putchar (c) ;
                      } while (c != '\n') ;
                 } else {		// ==> comentario, ignorar la linea
                      while (c != '\n') {
                          c = getchar () ;
                      }
                 }
             }
         }

         if (c == '\n')
             n_linea++ ;

    } while (c == ' ' || c == '\n' || c == 10 || c == 13 || c == '\t') ;

    if (c == '\"') {
         i = 0 ;
         do {
             c = getchar () ;
             cadena [i++] = c ;
         } while (c != '\"' && i < 255) ;
         if (i == 256) {
              printf ("AVISO: string con mas de 255 caracteres en linea %d\n", n_linea) ;
         }		 	// habria que leer hasta el siguient " , pero, y si falta?
         cadena [--i] = '\0' ;
         yylval.cadena = genera_cadena (cadena) ;
         return (STRING) ;
    }

    if (c == '.' || (c >= '0' && c <= '9')) {
         ungetc (c, stdin) ;
         scanf ("%d", &yylval.valor) ;
//         printf ("\nDEV: NUMERO %d\n", yylval.valor) ;        // PARA DEPURAR
         return NUMERO ;
    }

    if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
         i = 0 ;
         while (((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ||
                 (c >= '0' && c <= '9') || c == '_') && i < 255) {
             cadena [i++] = tolower (c) ;
             c = getchar () ;
         }
         cadena [i] = '\0' ;
         ungetc (c, stdin) ;

         yylval.cadena = genera_cadena (cadena) ;
         simbolo = busca_pal_reservada (yylval.cadena) ;
         if (simbolo == NULL) {    // no es palabra reservada -> identificador 
               //printf ("\nDEV: IDENTIF %s\n", yylval.cadena) ;    // PARA DEPURAR
               return (IDENTIF) ;
         } else {
               //printf ("\nDEV: RESERVADA %s\n", yylval.cadena) ;       // PARA DEPURAR
               return (simbolo->token) ;
         }
    }

    if (strchr (ops_expandibles, c) != NULL) { // busca c en ops_expandibles
         cc = getchar () ;
         sprintf (cadena, "%c%c", (char) c, (char) cc) ;
         simbolo = busca_pal_reservada (cadena) ;
         if (simbolo == NULL) {
              ungetc (cc, stdin) ;
              yylval.cadena = NULL ;
              return (c) ;
         } else {
              yylval.cadena = genera_cadena (cadena) ; // aunque no se use
              return (simbolo->token) ;
         }
    }

//    printf ("\nDEV: LITERAL %d #%c#\n", (int) c, c) ;      // PARA DEPURAR
    if (c == EOF || c == 255 || c == 26) {
//         printf ("tEOF ") ;                                // PARA DEPURAR
         return (0) ;
    }

    return c ;
}


int main ()
{
    yyparse () ;
}


