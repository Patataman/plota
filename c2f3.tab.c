/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "c2f3.y" /* yacc.c:339  */

#include <stdio.h>
#include <string.h>           // declaraciones para cadenas
#include <stdlib.h>           // declaraciones para exit ()
#define FF fflush(stdout);    // para forzar la impresion inmediata

char *nomb_func;
int contar = 0;
int contarArgs = 0;

//Tabla de simbolos
typedef struct {
    char nombre[30];
    char argumentos[2][30];
} Entrada;

Entrada tabla_simbolos[40];

/*
    Busca en la tabla de símbolos el nombre y devuelve 1 si está
    y un 0 si no está.
    
    func: Indica si se quiere buscar función (1) o varible (0)
*/
int searchVar (char *nombre) {
    if (contar == 0) { return 0; }
    int w = 0; 
    for (w=0;w<40 && (contar != 0 && contar < 40);w++) {
        if (strcmp(nombre,tabla_simbolos[w].nombre) == 0){
            return 1;
        }
    }

    return 0;
}

/*
    Busca en la tabla de símbolos el nombre y devuelve un 1 si está
    y un -1 si no está.

*/
int searchFunc (char *nombre, char *argu) {
    if (contar == 0) { return 0; }
    int w = 0; 
    for (w=0;w<40 && (contar != 0 && contar < 40);w++) {
        if (strcmp(nombre,tabla_simbolos[w].nombre) == 0){
            int argNum;
            for (argNum = 0; argNum < 2; argNum++){
                if (strcmp(argu,tabla_simbolos[w].argumentos[argNum]) == 0){
                    return 1;
                }
            }
            return -1;
        }
    }

    return -1;
}

void generarArgumentos() {
    int i;
    //Se lee el argumentos, si se llega a \0 es que tiene nombre válido, si se llega a 30, es que estaba vacío.
    for (i=0;i<3;i++){
        //Si se encuentra argumento válido
        if (tabla_simbolos[contar-1].argumentos[i][0] != '\0'){
            printf(" %s_%s !",nomb_func,tabla_simbolos[contar-1].argumentos[i]);
        }
    }
    printf("\n");
}

//HACER TABLA SIMBOLOS PARA VARIABLES GLOBALES

#line 140 "c2f3.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUMERO = 258,
    IDENTIF = 259,
    INTEGER = 260,
    STRING = 261,
    PUTS = 262,
    DO = 263,
    WHILE = 264,
    IF = 265,
    THEN = 266,
    ELSE = 267,
    OR = 268,
    AND = 269,
    EQUAL = 270,
    NEQUAL = 271,
    LT = 272,
    GT = 273,
    MASMAS = 274,
    MENOMENO = 275,
    PORIGUAL = 276,
    PRINTF = 277,
    FUNC = 278,
    RET = 279,
    VOID = 280,
    MAIN = 281,
    MASIGUAL = 282,
    MENOIGUAL = 283,
    SIGNO_UNARIO = 284
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 74 "c2f3.y" /* yacc.c:355  */
                      // El tipo de la pila tiene caracter dual
      int valor ;             // - valor numerico de un NUMERO
      char *cadena ;          // - para pasar los nombres de IDENTIFES

#line 212 "c2f3.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 227 "c2f3.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  7
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   812

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  49
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  61
/* YYNRULES -- Number of rules.  */
#define YYNRULES  122
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  309

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   284

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    39,     2,     2,     2,    38,    31,     2,
      41,    42,    36,    34,    48,    35,     2,    37,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    47,
      32,    29,    33,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    45,     2,    46,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    43,    30,    44,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    40
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   119,   119,   122,   123,   122,   126,   127,   127,   133,
     133,   139,   139,   150,   152,   153,   150,   156,   158,   158,
     156,   162,   163,   166,   169,   179,   180,   180,   183,   183,
     186,   186,   195,   196,   197,   198,   207,   208,   210,   211,
     208,   214,   216,   216,   214,   220,   221,   224,   224,   244,
     245,   245,   259,   265,   259,   273,   279,   273,   287,   293,
     287,   302,   302,   310,   310,   319,   319,   327,   327,   335,
     335,   337,   338,   339,   337,   343,   346,   343,   350,   350,
     357,   356,   362,   362,   366,   366,   367,   368,   367,   372,
     373,   374,   378,   379,   380,   381,   382,   383,   384,   385,
     386,   387,   388,   389,   390,   391,   392,   393,   394,   395,
     398,   399,   400,   403,   416,   417,   420,   427,   435,   435,
     445,   445,   453
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMERO", "IDENTIF", "INTEGER", "STRING",
  "PUTS", "DO", "WHILE", "IF", "THEN", "ELSE", "OR", "AND", "EQUAL",
  "NEQUAL", "LT", "GT", "MASMAS", "MENOMENO", "PORIGUAL", "PRINTF", "FUNC",
  "RET", "VOID", "MAIN", "MASIGUAL", "MENOIGUAL", "'='", "'|'", "'&'",
  "'<'", "'>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'!'", "SIGNO_UNARIO",
  "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "','", "$accept",
  "programa", "principal", "$@1", "$@2", "before_main", "$@3", "$@4",
  "$@5", "$@6", "$@7", "$@8", "$@9", "$@10", "$@11", "varM_globales",
  "var_locales", "$@12", "$@13", "$@14", "varM_locales", "def_func",
  "$@15", "$@16", "$@17", "$@18", "$@19", "$@20", "decl_args", "$@21",
  "codigo", "$@22", "$@23", "$@24", "$@25", "$@26", "$@27", "$@28", "$@29",
  "$@30", "$@31", "$@32", "$@33", "$@34", "$@35", "$@36", "$@37", "$@38",
  "$@39", "$@40", "$@41", "ifthenelse", "$@42", "$@43", "$@44", "param",
  "expresion", "termino", "operando", "$@45", "$@46", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,    61,
     124,    38,    60,    62,    43,    45,    42,    47,    37,    33,
     284,    40,    41,   123,   125,    91,    93,    59,    44
};
# endif

#define YYPACT_NINF -219

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-219)))

#define YYTABLE_NINF -48

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
       0,    11,    13,    27,    20,     9,  -219,  -219,  -219,  -219,
      29,  -219,    62,    51,  -219,    57,    60,    47,    43,   100,
       0,   100,    66,    52,   110,  -219,   112,    72,  -219,    79,
      84,   119,  -219,    86,    82,   107,   117,   150,   118,     0,
      59,   127,   150,   150,   161,  -219,   -26,  -219,   163,  -219,
     100,  -219,  -219,    78,   149,  -219,   132,  -219,   149,   149,
     176,  -219,   182,  -219,    14,   139,  -219,  -219,   146,   184,
     185,   151,   147,     0,   -26,   169,   152,   154,   104,   150,
    -219,  -219,  -219,    83,    83,    83,   189,   155,   156,    83,
     158,   159,   195,  -219,  -219,  -219,    83,  -219,   129,   199,
    -219,  -219,    83,    83,    83,  -219,   -27,   203,   204,    92,
      92,    83,   230,  -219,  -219,   167,   164,   446,   171,   149,
      83,   554,  -219,  -219,   166,   257,    15,   200,  -219,   165,
     284,   311,   338,    83,    83,  -219,  -219,  -219,  -219,   584,
      83,    83,    83,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,  -219,   168,    83,
     -21,   170,   174,   614,  -219,   149,   149,    83,  -219,   215,
     216,  -219,   175,   150,   137,  -219,  -219,  -219,   180,   473,
    -219,   714,   739,   102,   102,   109,   109,   704,   748,   773,
     109,   109,    12,    12,     6,     6,   186,   186,   149,  -219,
    -219,    83,    83,  -219,   218,  -219,   181,  -219,  -219,   644,
     188,  -219,  -219,     5,  -219,   227,  -219,   149,   149,   149,
    -219,   190,  -219,   149,   365,   500,   149,   198,   193,   149,
     202,    15,   209,   210,  -219,   206,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,   224,  -219,    83,   149,   211,  -219,  -219,
     100,   100,   150,     5,    83,   149,    83,   674,   212,   242,
     149,   234,   236,  -219,  -219,   527,  -219,   392,   232,  -219,
    -219,  -219,   149,  -219,   214,   237,  -219,  -219,  -219,   149,
     238,  -219,   150,   150,   149,   149,  -219,   149,  -219,  -219,
    -219,  -219,   239,   149,   149,  -219,   258,   240,   149,    83,
    -219,  -219,   419,    15,  -219,  -219,   241,    15,  -219
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       6,     0,     0,     0,     0,    13,    17,     1,     3,     2,
       0,    21,     0,     0,     7,     0,     0,     0,     0,    45,
       6,    45,     0,     0,     0,    22,     0,     0,     8,     0,
       0,     0,     9,     0,    46,     0,     0,    25,     0,     6,
       0,     0,    25,    25,     0,     4,     0,    10,     0,    23,
      45,    14,    18,     0,    49,    11,     0,    48,    49,    49,
       0,    32,     0,    26,     0,     0,    75,    71,     0,     0,
       0,     0,     0,     6,     0,     0,     0,     0,     0,    25,
      58,    52,    55,     0,    89,     0,     0,     0,     0,     0,
       0,     0,     0,     5,    12,    24,     0,    19,     0,     0,
      33,    27,     0,     0,     0,   114,   113,     0,     0,     0,
       0,     0,     0,    92,   110,     0,    90,     0,     0,    49,
       0,     0,    65,    67,     0,     0,    36,     0,    28,     0,
       0,     0,     0,    89,     0,   116,   117,   111,   112,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    50,     0,    89,
       0,     0,     0,     0,    80,    49,    49,     0,    15,     0,
       0,    20,     0,    25,     0,    59,    53,    56,     0,     0,
     115,    98,   100,   107,   109,   103,   104,   106,    97,    99,
     102,   101,    93,    94,    95,    96,   105,   108,    49,    82,
      91,     0,     0,    69,     0,    72,     0,    66,    68,     0,
       0,    37,    41,     0,    29,     0,    34,    49,    49,    49,
     122,   118,    51,    49,     0,     0,    49,     0,     0,    49,
       0,    36,     0,     0,    30,     0,    60,    54,    57,   120,
     119,    83,    61,     0,    70,     0,    49,     0,    78,    16,
      45,    45,    25,     0,     0,    49,     0,     0,     0,    84,
      49,     0,     0,    31,    35,     0,    62,     0,     0,    73,
      86,    81,    49,    79,     0,     0,   121,    63,    76,    49,
       0,    85,    25,    25,    49,    49,    74,    49,    38,    42,
      64,    77,     0,    49,    49,    87,     0,     0,    49,     0,
      43,    88,     0,    36,    39,    44,     0,    36,    40
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -219,  -219,  -219,  -219,  -219,   -13,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,   -12,   -42,  -219,  -219,  -219,
     -59,  -218,  -219,  -219,  -219,  -219,  -219,  -219,   -17,  -219,
     -56,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,  -219,
    -219,  -219,  -219,  -219,  -219,  -123,   -73,   -75,   -45,  -219,
    -219
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     3,     9,    16,    54,     4,    20,    39,    73,    13,
      58,   210,    15,    59,   126,    14,    45,    79,   173,   252,
      63,   171,   232,   293,   306,   233,   294,   303,    27,    41,
      72,   198,   103,   218,   104,   219,   102,   217,   255,   284,
     165,   166,   226,    88,   228,   279,    87,   285,   260,   206,
     223,   271,   272,   280,   298,   115,   112,   113,   114,   240,
     254
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      51,    52,    75,    76,    29,     1,    25,    28,   201,   116,
     178,    32,   117,   249,   133,     5,   121,     6,   134,   100,
     169,    11,    12,   125,   202,     2,    47,     7,    49,   130,
     131,   132,    17,    57,    55,    80,   200,   101,   139,   128,
     170,    81,    82,    83,   155,   156,     8,   163,   153,   154,
     155,   156,    61,    62,    10,    84,    11,    12,   116,    85,
      94,   179,    95,   162,   137,   138,    18,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   116,   305,   105,   106,    24,   308,
      11,    12,    19,    23,   209,   105,   106,    31,    21,    11,
      12,    22,   107,   108,    48,    26,    11,    12,    30,   207,
     208,   107,   108,    33,    35,   216,    34,   109,   110,   144,
     145,    36,    38,    60,   111,    61,    62,    37,   224,   225,
     -47,   214,    40,   111,   149,   150,   151,   152,   153,   154,
     155,   156,   222,   151,   152,   153,   154,   155,   156,    99,
      42,    61,    62,    64,   234,    44,    65,    66,    67,    68,
      43,   236,   237,   238,    46,    53,    56,   241,    69,    70,
     244,    71,   257,   247,   127,    50,    61,    62,    74,    77,
      86,   265,   215,   267,    61,    62,    78,    89,    90,    91,
     258,    93,    92,    96,   264,   118,    97,   120,   119,   266,
      98,   124,   129,   172,   273,   122,   123,   135,   136,   158,
     263,   174,   159,   161,   167,   199,   281,   203,   204,   211,
     212,   213,   220,   286,   229,   156,   302,   227,   290,   291,
     235,   292,   231,   261,   262,   239,   246,   296,   297,   245,
     288,   289,   301,   140,   141,   142,   143,   144,   145,   248,
     250,   251,   253,   256,   270,   259,   269,   282,     0,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     140,   141,   142,   143,   144,   145,   274,   157,   275,   278,
     283,   287,   299,   295,   300,   307,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   140,   141,   142,
     143,   144,   145,     0,   168,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   140,   141,   142,   143,   144,   145,
       0,   175,     0,     0,     0,     0,     0,     0,     0,     0,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   140,   141,   142,   143,   144,   145,     0,   176,     0,
       0,     0,     0,     0,     0,     0,     0,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   140,   141,
     142,   143,   144,   145,     0,   177,     0,     0,     0,     0,
       0,     0,     0,     0,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   140,   141,   142,   143,   144,
     145,     0,   242,     0,     0,     0,     0,     0,     0,     0,
       0,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   140,   141,   142,   143,   144,   145,     0,   277,
       0,     0,     0,     0,     0,     0,     0,     0,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   140,
     141,   142,   143,   144,   145,     0,   304,     0,     0,     0,
       0,     0,     0,     0,     0,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   140,   141,   142,   143,
     144,   145,   160,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   140,   141,   142,   143,   144,   145,   221,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     140,   141,   142,   143,   144,   145,   243,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   140,   141,   142,
     143,   144,   145,   276,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,   164,   140,   141,   142,
     143,   144,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,   180,   140,   141,   142,
     143,   144,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,   205,   140,   141,   142,
     143,   144,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,   230,   140,   141,   142,
     143,   144,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,   268,   140,   141,   142,
     143,   144,   145,     0,     0,     0,     0,     0,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   142,   143,   144,   145,     0,     0,
       0,     0,     0,   142,   143,   144,   145,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   142,   143,
     144,   145,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   149,   150,   151,   152,   153,
     154,   155,   156
};

static const yytype_int16 yycheck[] =
{
      42,    43,    58,    59,    21,     5,    18,    20,    29,    84,
     133,    23,    85,   231,    41,     4,    89,     4,    45,    78,
       5,    47,    48,    96,    45,    25,    39,     0,    40,   102,
     103,   104,     3,    50,    46,    21,   159,    79,   111,    98,
      25,    27,    28,    29,    38,    39,    26,   120,    36,    37,
      38,    39,    47,    48,    45,    41,    47,    48,   133,    45,
      73,   134,    74,   119,   109,   110,     4,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   159,   303,     3,     4,    45,   307,
      47,    48,    41,    46,   167,     3,     4,    45,    41,    47,
      48,    41,    19,    20,    45,     5,    47,    48,    42,   165,
     166,    19,    20,     3,    42,   174,     4,    34,    35,    17,
      18,    42,     3,    45,    41,    47,    48,    43,   201,   202,
      48,   173,    46,    41,    32,    33,    34,    35,    36,    37,
      38,    39,   198,    34,    35,    36,    37,    38,    39,    45,
      43,    47,    48,     4,   213,     5,     7,     8,     9,    10,
      43,   217,   218,   219,    46,     4,     3,   223,    19,    20,
     226,    22,   245,   229,    45,    48,    47,    48,    46,     3,
      41,   254,    45,   256,    47,    48,     4,    41,     4,     4,
     246,    44,    41,    24,   253,     6,    44,    41,    43,   255,
      46,     6,     3,     3,   260,    47,    47,     4,     4,    42,
     252,    46,    48,    42,    48,    47,   272,    47,    44,     4,
       4,    46,    42,   279,    43,    39,   299,     9,   284,   285,
       3,   287,    44,   250,   251,    45,    43,   293,   294,    41,
     282,   283,   298,    13,    14,    15,    16,    17,    18,    47,
      41,    41,    46,    29,    12,    44,    44,    43,    -1,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      13,    14,    15,    16,    17,    18,    42,    47,    42,    47,
      43,    43,    24,    44,    44,    44,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    13,    14,    15,
      16,    17,    18,    -1,    47,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    13,    14,    15,    16,    17,    18,
      -1,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    13,    14,    15,    16,    17,    18,    -1,    47,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    13,    14,
      15,    16,    17,    18,    -1,    47,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    13,    14,    15,    16,    17,
      18,    -1,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    13,    14,    15,    16,    17,    18,    -1,    47,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    13,
      14,    15,    16,    17,    18,    -1,    47,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    13,    14,    15,    16,
      17,    18,    46,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    13,    14,    15,    16,    17,    18,    46,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      13,    14,    15,    16,    17,    18,    46,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    13,    14,    15,
      16,    17,    18,    46,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    42,    13,    14,    15,
      16,    17,    18,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    42,    13,    14,    15,
      16,    17,    18,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    42,    13,    14,    15,
      16,    17,    18,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    42,    13,    14,    15,
      16,    17,    18,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    -1,    -1,    42,    13,    14,    15,
      16,    17,    18,    -1,    -1,    -1,    -1,    -1,    14,    15,
      16,    17,    18,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    15,    16,    17,    18,    -1,    -1,
      -1,    -1,    -1,    15,    16,    17,    18,    -1,    -1,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    15,    16,
      17,    18,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    32,    33,    34,    35,    36,
      37,    38,    39
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     5,    25,    50,    54,     4,     4,     0,    26,    51,
      45,    47,    48,    58,    64,    61,    52,     3,     4,    41,
      55,    41,    41,    46,    45,    64,     5,    77,    54,    77,
      42,    45,    64,     3,     4,    42,    42,    43,     3,    56,
      46,    78,    43,    43,     5,    65,    46,    54,    45,    64,
      48,    65,    65,     4,    53,    64,     3,    77,    59,    62,
      45,    47,    48,    69,     4,     7,     8,     9,    10,    19,
      20,    22,    79,    57,    46,    79,    79,     3,     4,    66,
      21,    27,    28,    29,    41,    45,    41,    95,    92,    41,
       4,     4,    41,    44,    54,    64,    24,    44,    46,    45,
      69,    65,    85,    81,    83,     3,     4,    19,    20,    34,
      35,    41,   105,   106,   107,   104,   106,   105,     6,    43,
      41,   105,    47,    47,     6,   105,    63,    45,    69,     3,
     105,   105,   105,    41,    45,     4,     4,   107,   107,   105,
      13,    14,    15,    16,    17,    18,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    47,    42,    48,
      46,    42,    79,   105,    42,    89,    90,    48,    47,     5,
      25,    70,     3,    67,    46,    47,    47,    47,   104,   105,
      42,   105,   105,   105,   105,   105,   105,   105,   105,   105,
     105,   105,   105,   105,   105,   105,   105,   105,    80,    47,
     104,    29,    45,    47,    44,    42,    98,    79,    79,   105,
      60,     4,     4,    46,    65,    45,    69,    86,    82,    84,
      42,    46,    79,    99,   105,   105,    91,     9,    93,    43,
      42,    44,    71,    74,    69,     3,    79,    79,    79,    45,
     108,    79,    47,    46,    79,    41,    43,    79,    47,    70,
      41,    41,    68,    46,   109,    87,    29,   105,    79,    44,
      97,    77,    77,    65,    69,   105,    79,   105,    42,    44,
      12,   100,   101,    79,    42,    42,    46,    47,    47,    94,
     102,    79,    43,    43,    88,    96,    79,    43,    65,    65,
      79,    79,    79,    72,    75,    44,    79,    79,   103,    24,
      44,    79,   105,    76,    47,    70,    73,    44,    70
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    49,    50,    52,    53,    51,    54,    55,    54,    56,
      54,    57,    54,    58,    59,    60,    54,    61,    62,    63,
      54,    64,    64,    64,    64,    65,    66,    65,    67,    65,
      68,    65,    69,    69,    69,    69,    70,    71,    72,    73,
      70,    74,    75,    76,    70,    77,    77,    78,    77,    79,
      80,    79,    81,    82,    79,    83,    84,    79,    85,    86,
      79,    87,    79,    88,    79,    89,    79,    90,    79,    91,
      79,    92,    93,    94,    79,    95,    96,    79,    97,    79,
      98,    79,    99,    79,   101,   100,   102,   103,   100,   104,
     104,   104,   105,   105,   105,   105,   105,   105,   105,   105,
     105,   105,   105,   105,   105,   105,   105,   105,   105,   105,
     106,   106,   106,   107,   107,   107,   107,   107,   108,   107,
     109,   107,   107
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     0,     0,     9,     0,     0,     5,     0,
       8,     0,    11,     0,     0,     0,    16,     0,     0,     0,
      13,     1,     3,     6,     9,     0,     0,     5,     0,     8,
       0,    11,     1,     3,     6,     9,     0,     0,     0,     0,
      16,     0,     0,     0,    13,     0,     2,     0,     5,     0,
       0,     6,     0,     0,     7,     0,     0,     7,     0,     0,
       7,     0,     9,     0,    12,     0,     5,     0,     5,     0,
       7,     0,     0,     0,    11,     0,     0,    12,     0,     9,
       0,     9,     0,     7,     0,     2,     0,     0,     7,     0,
       1,     3,     1,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       1,     2,     2,     1,     1,     3,     2,     2,     0,     5,
       0,     8,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 119 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 1611 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 122 "c2f3.y" /* yacc.c:1646  */
    {nomb_func = (yyvsp[0].cadena);}
#line 1617 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 123 "c2f3.y" /* yacc.c:1646  */
    { printf (": main \n"); FF; }
#line 1623 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 123 "c2f3.y" /* yacc.c:1646  */
    { printf (" ;\n"); FF; }
#line 1629 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 126 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 1635 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 127 "c2f3.y" /* yacc.c:1646  */
    { strcpy(tabla_simbolos[contar].nombre,(yyvsp[-1].cadena));
                                          contar++;
                                          printf("variable %s\n", (yyvsp[-1].cadena)); FF; }
#line 1643 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 133 "c2f3.y" /* yacc.c:1646  */
    { strcpy(tabla_simbolos[contar].nombre,(yyvsp[-4].cadena));
                                                    contar++;
                                                    printf("variable %s %d cells allot\n", (yyvsp[-4].cadena), (yyvsp[-2].valor));}
#line 1651 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 139 "c2f3.y" /* yacc.c:1646  */
    {   strcpy(tabla_simbolos[contar].nombre,(yyvsp[-7].cadena));
                                                                    contar++;
                                                                    printf("variable %s %d %d * %d + cells allot\n", (yyvsp[-7].cadena), (yyvsp[-5].valor), (yyvsp[-2].valor), (yyvsp[-5].valor));
                                                                    int i;
                                                                    for (i = 0 ; i < (yyvsp[-5].valor) ; i++) {
                                                                     printf (" %d  %d %d *  + cells %s + %d cells %s + !\n", (yyvsp[-5].valor), i, (yyvsp[-2].valor), (yyvsp[-7].cadena), i, (yyvsp[-7].cadena)) ;
                                                                    } 
                                                                }
#line 1664 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 150 "c2f3.y" /* yacc.c:1646  */
    {nomb_func = (yyvsp[0].cadena); strcpy(tabla_simbolos[contar].nombre,(yyvsp[0].cadena));}
#line 1670 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 152 "c2f3.y" /* yacc.c:1646  */
    { printf (": %s ", (yyvsp[-6].cadena)); generarArgumentos(); }
#line 1676 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 153 "c2f3.y" /* yacc.c:1646  */
    {printf(" ;\n"); }
#line 1682 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 156 "c2f3.y" /* yacc.c:1646  */
    {nomb_func = (yyvsp[0].cadena); strcpy(tabla_simbolos[contar].nombre,(yyvsp[0].cadena));}
#line 1688 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 158 "c2f3.y" /* yacc.c:1646  */
    { printf (": %s", (yyvsp[-6].cadena)); generarArgumentos(); }
#line 1694 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 158 "c2f3.y" /* yacc.c:1646  */
    { printf(" ;\n"); }
#line 1700 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 163 "c2f3.y" /* yacc.c:1646  */
    { strcpy(tabla_simbolos[contar].nombre,(yyvsp[-1].cadena));
                                          contar++;
                                          printf("variable %s\n", (yyvsp[-1].cadena)); FF; }
#line 1708 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 166 "c2f3.y" /* yacc.c:1646  */
    { strcpy(tabla_simbolos[contar].nombre,(yyvsp[-4].cadena));
                                                    contar++;
                                                    printf("variable %s %d cells allot\n", (yyvsp[-4].cadena), (yyvsp[-2].valor));}
#line 1716 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 169 "c2f3.y" /* yacc.c:1646  */
    { strcpy(tabla_simbolos[contar].nombre,(yyvsp[-7].cadena));
                                                                        contar++;
                                                                        printf("variable %s %d %d * %d + cells allot\n", (yyvsp[-7].cadena), (yyvsp[-5].valor), (yyvsp[-2].valor), (yyvsp[-5].valor));
                                                                        int i;
                                                                        for (i = 0 ; i < (yyvsp[-5].valor) ; i++) {
                                                                            printf (" %d %d %d *  + cells %s + %d cells %s + !\n", (yyvsp[-5].valor), i, (yyvsp[-2].valor), (yyvsp[-7].cadena), i, (yyvsp[-7].cadena)) ;
                                                                        } 
                                                                    }
#line 1729 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 179 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 1735 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 180 "c2f3.y" /* yacc.c:1646  */
    { printf ("variable %s_%s\n", nomb_func,(yyvsp[-1].cadena)); FF; }
#line 1741 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 183 "c2f3.y" /* yacc.c:1646  */
    {printf("variable %s_%s %d cells allot\n",nomb_func, (yyvsp[-4].cadena), (yyvsp[-2].valor));}
#line 1747 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 186 "c2f3.y" /* yacc.c:1646  */
    {printf("variable %s_%s %d %d * %d + cells allot\n",nomb_func, (yyvsp[-7].cadena), (yyvsp[-5].valor), (yyvsp[-2].valor), (yyvsp[-5].valor));
                                                                            int i;
                                                                            for (i = 0 ; i < (yyvsp[-5].valor) ; i++) {
                                                                                printf (" %d %d %d *  + cells %s_%s + %d cells %s + !\n", (yyvsp[-5].valor), i, (yyvsp[-2].valor), nomb_func, (yyvsp[-7].cadena), i, nomb_func, (yyvsp[-7].cadena)) ;
                                                                            } 
                                                                        }
#line 1758 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 196 "c2f3.y" /* yacc.c:1646  */
    { printf ("variable %s_%s\n", nomb_func,(yyvsp[-1].cadena)); FF; }
#line 1764 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 197 "c2f3.y" /* yacc.c:1646  */
    {printf("variable %s_%s %d cells allot\n",nomb_func, (yyvsp[-4].cadena), (yyvsp[-2].valor));}
#line 1770 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 198 "c2f3.y" /* yacc.c:1646  */
    {printf("variable %s_%s %d %d * %d + cells allot\n",nomb_func, (yyvsp[-7].cadena), (yyvsp[-5].valor), (yyvsp[-2].valor), (yyvsp[-5].valor));
                                                                        int i;
                                                                        for (i = 0 ; i < (yyvsp[-5].valor) ; i++) {
                                                                            printf (" %d %d %d *  + cells %s_%s + %d cells %s + !\n", (yyvsp[-5].valor), i, (yyvsp[-2].valor), nomb_func, (yyvsp[-7].cadena), i, nomb_func, (yyvsp[-7].cadena)) ;
                                                                        } 
                                                                    }
#line 1781 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 207 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 1787 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 208 "c2f3.y" /* yacc.c:1646  */
    {nomb_func = (yyvsp[0].cadena); strcpy(tabla_simbolos[contar].nombre,(yyvsp[0].cadena));}
#line 1793 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 210 "c2f3.y" /* yacc.c:1646  */
    { printf (": %s ", (yyvsp[-6].cadena)); generarArgumentos(); }
#line 1799 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 211 "c2f3.y" /* yacc.c:1646  */
    {printf(" ;\n"); FF; }
#line 1805 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 214 "c2f3.y" /* yacc.c:1646  */
    {nomb_func = (yyvsp[0].cadena); strcpy(tabla_simbolos[contar].nombre,(yyvsp[0].cadena));}
#line 1811 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 216 "c2f3.y" /* yacc.c:1646  */
    { printf (": %s", (yyvsp[-6].cadena)); generarArgumentos(); }
#line 1817 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 216 "c2f3.y" /* yacc.c:1646  */
    { printf(" ;\n"); }
#line 1823 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 220 "c2f3.y" /* yacc.c:1646  */
    { contar++; }
#line 1829 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 221 "c2f3.y" /* yacc.c:1646  */
    {strcpy(tabla_simbolos[contar].argumentos[contarArgs],(yyvsp[0].cadena));
                                contarArgs=0; contar++; 
                                printf ("variable  %s_%s\n", nomb_func, (yyvsp[0].cadena));}
#line 1837 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 224 "c2f3.y" /* yacc.c:1646  */
    {strcpy(tabla_simbolos[contar].argumentos[contarArgs],(yyvsp[0].cadena));
                                contarArgs++; 
                                printf ("variable %s_%s ", nomb_func, (yyvsp[0].cadena));}
#line 1845 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 244 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 1851 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 245 "c2f3.y" /* yacc.c:1646  */
    { if (searchFunc(nomb_func, (yyvsp[-3].cadena)) == 1) {
                                                printf (" %s_%s !\n", nomb_func, (yyvsp[-3].cadena));
                                              }
                                              else {
                                                    if(searchVar((yyvsp[-3].cadena)) == 1){
                                                      printf(" %s !\n", (yyvsp[-3].cadena));
                                                    } else {
                                                      printf (" %s_%s !\n", nomb_func, (yyvsp[-3].cadena));
                                                    }
                                                }
                                            }
#line 1867 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 259 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-1].cadena)) == 1){
                                    printf(" %s @ ", (yyvsp[-1].cadena));
                                  } else {
                                    printf (" %s_%s @ ", nomb_func, (yyvsp[-1].cadena));
                                  }
                                }
#line 1878 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 265 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-4].cadena)) == 1){
                                    printf(" + %s !\n", (yyvsp[-4].cadena));
                                  } else {
                                    printf (" + %s_%s !\n", nomb_func, (yyvsp[-4].cadena));
                                  }
                                }
#line 1889 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 273 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-1].cadena)) == 1){
                                    printf(" %s @ ", (yyvsp[-1].cadena));
                                    } else {
                                        printf (" %s_%s @ ", nomb_func, (yyvsp[-1].cadena));
                                    }
                                  }
#line 1900 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 279 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-4].cadena)) == 1){
                                    printf(" - %s !\n", (yyvsp[-4].cadena));
                                  } else {
                                    printf (" - %s_%s !\n", nomb_func, (yyvsp[-4].cadena));
                                  }
                                }
#line 1911 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 287 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-1].cadena)) == 1){
                                    printf(" %s @ ", (yyvsp[-1].cadena));
                                    } else {
                                        printf (" %s_%s @ ", nomb_func, (yyvsp[-1].cadena));
                                    }
                                  }
#line 1922 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 293 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-4].cadena)) == 1){
                                    printf(" * %s !\n", (yyvsp[-4].cadena));
                                  } else {
                                    printf (" * %s_%s !\n", nomb_func, (yyvsp[-4].cadena));
                                  }
                                }
#line 1933 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 302 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-6].cadena)) == 1){
                                                            printf(" %s rot cells + !\n", (yyvsp[-6].cadena));
                                                          } else {
                                                            printf (" %s_%s rot cells + !\n", nomb_func, (yyvsp[-6].cadena));
                                                          }
                                                        }
#line 1944 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 310 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-9].cadena)) == 1){
                                                                            printf(" rot rot swap cells %s + @ swap cells + !\n", (yyvsp[-9].cadena));
                                                                          } else {
                                                                            printf (" rot rot swap cells %s_%s + @ swap cells + !\n", nomb_func, (yyvsp[-9].cadena));
                                                                          }
                                                                        }
#line 1955 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 319 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-1].cadena)) == 1){
                                        printf(" %s @ 1 + %s !\n", (yyvsp[-1].cadena), (yyvsp[-1].cadena));
                                      } else {
                                        printf (" %s_%s @ 1 + %s_%s !\n", nomb_func, (yyvsp[-1].cadena), nomb_func, (yyvsp[-1].cadena));
                                      }
                                    }
#line 1966 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 327 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-1].cadena)) == 1){
                                            printf(" %s @ 1 - %s !\n", (yyvsp[-1].cadena), (yyvsp[-1].cadena));
                                          } else {
                                            printf (" %s_%s @ 1 - %s_%s !\n", nomb_func, (yyvsp[-1].cadena), nomb_func, (yyvsp[-1].cadena));
                                          }
                                        }
#line 1977 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 335 "c2f3.y" /* yacc.c:1646  */
    { printf (" .\" %s \"\n", (yyvsp[-2].cadena)) ; }
#line 1983 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 337 "c2f3.y" /* yacc.c:1646  */
    { printf (" begin ") ; }
#line 1989 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 338 "c2f3.y" /* yacc.c:1646  */
    { printf (" while\n  ") ; }
#line 1995 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 339 "c2f3.y" /* yacc.c:1646  */
    { printf (" repeat \n") ; }
#line 2001 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 343 "c2f3.y" /* yacc.c:1646  */
    { printf (" begin ") ; }
#line 2007 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 346 "c2f3.y" /* yacc.c:1646  */
    { printf (" while\n repeat\n") ; }
#line 2013 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 350 "c2f3.y" /* yacc.c:1646  */
    { printf (" .\n") ; }
#line 2019 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 357 "c2f3.y" /* yacc.c:1646  */
    { printf (" if ") ; }
#line 2025 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 362 "c2f3.y" /* yacc.c:1646  */
    { printf(" %s\n", (yyvsp[-4].cadena) ); }
#line 2031 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 366 "c2f3.y" /* yacc.c:1646  */
    { printf (" then \n") ; }
#line 2037 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 367 "c2f3.y" /* yacc.c:1646  */
    { printf (" else ") ; }
#line 2043 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 368 "c2f3.y" /* yacc.c:1646  */
    { printf (" then \n") ; }
#line 2049 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 372 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2055 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 373 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2061 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 374 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2067 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 378 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2073 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 379 "c2f3.y" /* yacc.c:1646  */
    { printf (" +") ; }
#line 2079 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 380 "c2f3.y" /* yacc.c:1646  */
    { printf (" -") ; }
#line 2085 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 381 "c2f3.y" /* yacc.c:1646  */
    { printf (" *") ; }
#line 2091 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 382 "c2f3.y" /* yacc.c:1646  */
    { printf (" /") ; }
#line 2097 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 383 "c2f3.y" /* yacc.c:1646  */
    { printf (" or") ; }
#line 2103 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 384 "c2f3.y" /* yacc.c:1646  */
    { printf (" or") ; }
#line 2109 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 385 "c2f3.y" /* yacc.c:1646  */
    { printf (" and") ; }
#line 2115 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 386 "c2f3.y" /* yacc.c:1646  */
    { printf (" and") ; }
#line 2121 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 387 "c2f3.y" /* yacc.c:1646  */
    { printf (" >") ; }
#line 2127 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 388 "c2f3.y" /* yacc.c:1646  */
    { printf (" <") ; }
#line 2133 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 389 "c2f3.y" /* yacc.c:1646  */
    { printf (" <=") ; }
#line 2139 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 390 "c2f3.y" /* yacc.c:1646  */
    { printf (" >=") ; }
#line 2145 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 391 "c2f3.y" /* yacc.c:1646  */
    { printf (" mod") ; }
#line 2151 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 392 "c2f3.y" /* yacc.c:1646  */
    { printf (" =") ; }
#line 2157 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 393 "c2f3.y" /* yacc.c:1646  */
    { printf (" =") ; }
#line 2163 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 394 "c2f3.y" /* yacc.c:1646  */
    { printf (" 0") ; }
#line 2169 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 395 "c2f3.y" /* yacc.c:1646  */
    { printf (" = 0=") ; }
#line 2175 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 398 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2181 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 399 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2187 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 400 "c2f3.y" /* yacc.c:1646  */
    { printf ("negate ") ; }
#line 2193 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 403 "c2f3.y" /* yacc.c:1646  */
    { if (searchFunc(nomb_func, (yyvsp[0].cadena)) == 1) {
                                            printf (" %s_%s @", nomb_func, (yyvsp[0].cadena));
                                        }
                                  else {
                                        if(searchVar((yyvsp[0].cadena)) == 1){
                                          printf(" %s @", (yyvsp[0].cadena));
                                        } else {
                                          printf (" %s_%s @", nomb_func, (yyvsp[0].cadena));
                                        }
                                    }
                                }
#line 2209 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 416 "c2f3.y" /* yacc.c:1646  */
    { printf (" %d", (yyvsp[0].valor)) ; }
#line 2215 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 417 "c2f3.y" /* yacc.c:1646  */
    { ; }
#line 2221 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 116:
#line 420 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[0].cadena)) == 1){
                                        printf(" %s @ 1 + %s ! %s @", (yyvsp[0].cadena), (yyvsp[0].cadena), (yyvsp[0].cadena));
                                      } else {
                                        printf (" %s_%s @ 1 + %s_%s ! %s_%s @", nomb_func, (yyvsp[0].cadena), nomb_func, (yyvsp[0].cadena), nomb_func, (yyvsp[0].cadena));
                                      }
                                    }
#line 2232 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 427 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[0].cadena)) == 1){
                                        printf(" %s @ 1 - %s !", (yyvsp[0].cadena), (yyvsp[0].cadena));
                                      } else {
                                        printf (" %s_%s @ 1 - %s_%s !", nomb_func, (yyvsp[0].cadena), nomb_func, (yyvsp[0].cadena));
                                      }
                                    }
#line 2243 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 435 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-3].cadena)) == 1){
                                                printf(" %s", (yyvsp[-3].cadena));
                                              } else {
                                                printf (" %s_%s", nomb_func, (yyvsp[-3].cadena));
                                              }
                                              printf(" swap");
                                            }
#line 2255 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 442 "c2f3.y" /* yacc.c:1646  */
    { printf(" cells + @"); }
#line 2261 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 445 "c2f3.y" /* yacc.c:1646  */
    { if(searchVar((yyvsp[-4].cadena)) == 1){
                                                printf(" cells %s + @", (yyvsp[-4].cadena));
                                              } else {
                                                printf (" cells %s_%s + @", nomb_func, (yyvsp[-4].cadena));
                                              }
                                            }
#line 2272 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 451 "c2f3.y" /* yacc.c:1646  */
    { printf(" cells + @"); }
#line 2278 "c2f3.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 453 "c2f3.y" /* yacc.c:1646  */
    { printf(" %s", (yyvsp[-3].cadena) ); }
#line 2284 "c2f3.tab.c" /* yacc.c:1646  */
    break;


#line 2288 "c2f3.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 457 "c2f3.y" /* yacc.c:1906  */

                            // SECCION 4    Codigo en C
int n_linea = 1 ;

int yyerror (mensaje)
char *mensaje ;
{
    fprintf (stderr, "%s en la linea %d\n", mensaje, n_linea) ;
    printf ( "bye\n") ;
}

char *mi_malloc (int nbytes)       // reserva n bytes de memoria dinamica
{
    char *p ;
    static long int nb = 0;        // sirven para contabilizar la memoria
    static int nv = 0 ;            // solicitada en total

    p = malloc (nbytes) ;
    if (p == NULL) {
         fprintf (stderr, "No queda memoria para %d bytes mas\n", nbytes) ;
         fprintf (stderr, "Reservados %ld bytes en %d llamadas\n", nb, nv) ;
         exit (0) ;
    }
    nb += (long) nbytes ;
    nv++ ;

    return p ;
}


/***************************************************************************/
/********************** Seccion de Palabras Reservadas *********************/
/***************************************************************************/

typedef struct s_pal_reservadas { // para las palabras reservadas de C
    char *nombre ;
    int token ;
} t_reservada ;

t_reservada pal_reservadas [] = { // define las palabras reservadas y los
    "main",        MAIN,           // y los token asociados
    "int",         INTEGER,
    "puts",        PUTS,
    "while",	   WHILE,
    "do",          DO, //////////////////////// AÑADIMOS COMO PALABRA RESERVADA 'DO'
    "if",          IF,
    "then",        THEN,
    "else",        ELSE,
    "&&",          AND,
    "||",          OR,
    "<=",          LT, //(less than)
    ">=",          GT, //(greater than)
    "==",          EQUAL,
    "!=",          NEQUAL,
    "++",          MASMAS,
    "+=",          MASIGUAL,
    "--",          MENOMENO,
    "-=",          MENOIGUAL,
    "*=",          PORIGUAL,
    "printf",      PRINTF,
    "return",      RET,
    "void",        VOID,
    NULL,          0               // para marcar el fin de la tabla
} ;

t_reservada *busca_pal_reservada (char *nombre_simbolo)
{                                  // Busca n_s en la tabla de pal. res.
                                   // y devuelve puntero a registro (simbolo)
    int i ;
    t_reservada *sim ;

    i = 0 ;
    sim = pal_reservadas ;
    while (sim [i].nombre != NULL) {
           if (strcmp (sim [i].nombre, nombre_simbolo) == 0) {
                                         // strcmp(a, b) devuelve == 0 si a==b
                 return &(sim [i]) ;
             }
           i++ ;
    }

    return NULL ;
}

 
/***************************************************************************/
/******************* Seccion del Analizador Lexicografico ******************/
/***************************************************************************/

char *genera_cadena (char *nombre)     // copia el argumento a un
{                                      // string en memoria dinamica
      char *p ;
      int l ;

      l = strlen (nombre)+1 ;
      p = (char *) mi_malloc (l) ;
      strcpy (p, nombre) ;

      return p ;
}


int yylex ()
{
    int i ;
    unsigned char c ;
    unsigned char cc ;
    char ops_expandibles [] = "=!<>&|%+-*/" ;
    char cadena [256] ;
    t_reservada *simbolo ;
    char func [256] ; //Para conocer el nombre de la funcion en cuestion

    do {
         c = getchar () ;

         if (c == '#') {	// Ignora las líneas que empiezan por #  (#define, #include)
             do {		//	OJO que puede funcionar mal si una linea contiene #
                 c = getchar () ;	
             } while (c != '\n') ;
         }

         if (c == '/') {	// Si la linea contiene un / puede ser inicio de comentario
             cc = getchar () ;
             if (cc != '/') {   // Si el siguiente char es /  es un comentario, pero...
                 ungetc (cc, stdin) ;
             } else {
                 c = getchar () ;	// ...
                 if (c == '@') {	// Si es la secuencia //@  ==> transcribimos la linea
                      do {		// Se trata de codigo inline (Codigo Forth embebido en C)
                          c = getchar () ;
                          putchar (c) ;
                      } while (c != '\n') ;
                 } else {		// ==> comentario, ignorar la linea
                      while (c != '\n') {
                          c = getchar () ;
                      }
                 }
             }
         }

         if (c == '\n')
             n_linea++ ;

    } while (c == ' ' || c == '\n' || c == 10 || c == 13 || c == '\t') ;

    if (c == '\"') {
         i = 0 ;
         do {
             c = getchar () ;
             cadena [i++] = c ;
         } while (c != '\"' && i < 255) ;
         if (i == 256) {
              printf ("AVISO: string con mas de 255 caracteres en linea %d\n", n_linea) ;
         }		 	// habria que leer hasta el siguient " , pero, y si falta?
         cadena [--i] = '\0' ;
         yylval.cadena = genera_cadena (cadena) ;
         return (STRING) ;
    }

    if (c == '.' || (c >= '0' && c <= '9')) {
         ungetc (c, stdin) ;
         scanf ("%d", &yylval.valor) ;
//         printf ("\nDEV: NUMERO %d\n", yylval.valor) ;        // PARA DEPURAR
         return NUMERO ;
    }

    if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
         i = 0 ;
         while (((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ||
                 (c >= '0' && c <= '9') || c == '_') && i < 255) {
             cadena [i++] = tolower (c) ;
             c = getchar () ;
         }
         cadena [i] = '\0' ;
         ungetc (c, stdin) ;

         yylval.cadena = genera_cadena (cadena) ;
         simbolo = busca_pal_reservada (yylval.cadena) ;
         if (simbolo == NULL) {    // no es palabra reservada -> identificador 
               //printf ("\nDEV: IDENTIF %s\n", yylval.cadena) ;    // PARA DEPURAR
               return (IDENTIF) ;
         } else {
               //printf ("\nDEV: RESERVADA %s\n", yylval.cadena) ;       // PARA DEPURAR
               return (simbolo->token) ;
         }
    }

    if (strchr (ops_expandibles, c) != NULL) { // busca c en ops_expandibles
         cc = getchar () ;
         sprintf (cadena, "%c%c", (char) c, (char) cc) ;
         simbolo = busca_pal_reservada (cadena) ;
         if (simbolo == NULL) {
              ungetc (cc, stdin) ;
              yylval.cadena = NULL ;
              return (c) ;
         } else {
              yylval.cadena = genera_cadena (cadena) ; // aunque no se use
              return (simbolo->token) ;
         }
    }

//    printf ("\nDEV: LITERAL %d #%c#\n", (int) c, c) ;      // PARA DEPURAR
    if (c == EOF || c == 255 || c == 26) {
//         printf ("tEOF ") ;                                // PARA DEPURAR
         return (0) ;
    }

    return c ;
}


int main ()
{
    yyparse () ;
}


